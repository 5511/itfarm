<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title> ${config.webTitle} </title>
  <%@include file="/WEB-INF/layouts/basejs.jsp"%>
  <link type="text/css" rel="stylesheet" href="${ctx}/css/login.css">
  <script type="text/javascript">
    $(document).ready(function(e) {
      /*使导航栏一直处于顶部*/
      var _nav = $(".topcool");
      $(document).scroll(function () {
        var t = document.documentElement.scrollTop || document.body.scrollTop;
        if (t > 50) {
          _nav.css("position", "fixed");
          _nav.css("margin", "0");
          _nav.css("top", "0");
          _nav.css("opacity", "0.9");
        }
        else {
          _nav.css("position", "relative");
          _nav.css("opacity", "1");
        }
      });

      $(".navmore").hover(function () {
        $(this).addClass("navmore-hover");
        $(this).find(".navMoreUL").show();
      }, function () {
        $(this).removeClass("navmore-hover");
        $(this).find(".navMoreUL").hide();
      });

      $(".topSearch").hover(function () {
        $(this).find(".chooseS").show();
      }, function () {
        $(this).find(".chooseS").hide();
      });
      /*登陆窗口*/
      $("#login_form").dialog({
        autoOpen: false,
        resizable : false,
        width: 500,
        height : 300,
        position : ['center','top'],
        modal : true
      });

      $("#loginbtn").click(function () {
        $("#login_form").dialog("open");
      });

      /*注册窗口*/
      $("#register_form").dialog({
        autoOpen: false,
        resizable : false,
        position : ['center','top'],
        width: 600,
        height : 450,
        modal : true
      });

      $("#regbtn").click(function () {
        $("#register_form").dialog("open");
      });

      $("#to_register").click(function () {
        $("#register_form").dialog("open");
        $("#login_form").dialog("close");
      });

      $("#to_login").click(function () {
        $("#login_form").dialog("open");
        $("#register_form").dialog("close");
      });


      /*登录事件*/
      $("#login_btn").click(function () {
        $.post('${ctx}/login/loginDo.do',
                {
                  username : $("#username").val(),
                  password : $("#password").val()
                }, function (data) {
                  if (data.success) {
                    $("#loginbtn").text(data.result.nickname);
                    $("#loginbtn").attr("id", "nickname");
                    $("#regbtn").text("注销");
                    $("#regbtn").attr("id", "login_out");
                    $("#login_form").dialog("close");
                  } else {
                    $("#errorMsg").text(data.resultMessage);
                  }
                });

      });

      /*注销事件*/
      $("#login_out").on("click", function () {
        $.post('${ctx}/login/loginOut.do',
                function (data) {
                  if (data.success) {
                    $("#nickname").text("登录");
                    $("#nickname").attr("id", "loginbtn");
                    $("#login_out").text("注册");
                    $("#login_out").attr("id", "regbtn");
                    $("#login_form").dialog("close");
                  } else {
                    $("#errorMsg").text(data.resultMessage);
                  }
                });
      });
    });

  </script>
</head>
<body>
<div class="topcool">
  <div class="topheader"> <a href="#" class="toplogo"></a>
    <div class="topnav">
      <a accesskey="false">ITFARM</a>
      <c:forEach items="${menus}" var="menu" varStatus="vs">
        <a <c:if test="${menuSelected == vs.count}">class="selected"</c:if> href="${ctx}${menu.url}">${menu.name}</a>
      </c:forEach>
      <span class="navmore">更多<b></b>
      <ul class="navMoreUL">
        <li><a href="${config.adminUrl}" target="_blank">后台管理</a></li>
      </ul>
      </span> </div>
    <div class="topheadright">
      <ul class="thrUl">
        <li class="doLogBox">
          <c:if test="${user != null}">
          <a href="javascript:" id="nickname">${user.nickname}</a>|<a href="javascript:" id="login_out">注销</a>
          </c:if>
          <c:if test="${user == null}">
            <a href="javascript:" id="loginbtn">登录</a>|<a href="javascript:" id="regbtn">注册</a>
          </c:if>
        </li>
      </ul>
      <span class="topSearch">
      <form action="" method="get">
        <input type="text" name="world" class="topSearchTxt">
        <input type="submit" class="topSearchBtn" value="">
        <div class="chooseS layout">
          <table width="100%">
            <tbody>
              <tr>
                <td><input type="radio" name="page" id="a1" value="" checked="">
                  <label for="a1">全部</label></td>
                <c:forEach items="${menus}" var="menu" varStatus="vs">
                <td><input type="radio" name="page" id="a2" value="${menu.recordId}">
                  <label for="a2">${menu.name}</label></td>
                </c:forEach>
              </tr>
            </tbody>
          </table>
        </div>
      </form>
      </span> </div>
  </div>
</div>
<%--登陆弹出框--%>
<div class="zhuce_kong login_kuang" id="login_form" style="display: none">
    <div class="zc">
      <div class="bj_bai">
        <h3>登录&nbsp;&nbsp;<a style="color: red" id="errorMsg"></a></h3>
        <form action="${ctx}/login/login.do" method="post" id="login_submit">
          <input name="username" id="username" type="text" class="kuang_txt" placeholder="用户名">
          <input name="password" id="password" type="text" class="kuang_txt" placeholder="密码">
          <div>
            <a href="#">忘记密码？</a><input name="" type="checkbox" value="" checked><span>记住我</span>
          </div>
          <input name="登录" id="login_btn" type="button" class="btn_zhuce" value="登录">

        </form>
      </div>
      <div class="bj_right">
        <p>使用以下账号直接登录</p>
        <a href="#" class="zhuce_qq">QQ注册</a>
        <a href="#" class="zhuce_wb">微博注册</a>
        <a href="#" class="zhuce_wx">微信注册</a>
        <p>还未注册？<a href="javascript;" id="to_register">立即注册</a></p>

      </div>
    </div>

</div>
<%--注册弹出框--%>
<div class="zhuce_kong" id="register_form" style="display: none">
  <div class="zc">
    <div class="bj_bai">
      <h3>欢迎注册</h3>
      <form action="" method="get">
        <input name="" type="text" class="kuang_txt phone" placeholder="用户名">
        <input name="" type="text" class="kuang_txt email" placeholder="邮箱">
        <input name="" type="text" class="kuang_txt possword" placeholder="密码">
        <input name="" type="text" class="kuang_txt possword" placeholder="邀请码">
        <input name="" type="text" class="kuang_txt yanzm" placeholder="验证码">
        <div>
          <div class="hui_kuang"><img src="${ctx}/images/zc_22.jpg" width="92" height="31"></div>
          <div class="shuaxin"><a href="#"><img src="${ctx}/images/zc_25.jpg" width="13" height="14"></a></div>
        </div>
        <div>
          <input name="" type="checkbox" value=""><span>已阅读并同意<a href="#" target="_blank"><span class="lan">《XXXXX使用协议》</span></a></span>
        </div>
        <input name="注册" type="button" class="btn_zhuce" value="注册">

      </form>
    </div>
    <div class="bj_right">
      <p>使用以下账号直接登录</p>
      <a href="#" class="zhuce_qq">QQ注册</a>
      <a href="#" class="zhuce_wb">微博注册</a>
      <a href="#" class="zhuce_wx">微信注册</a>
      <p>已有账号？<a href="javascript;" id="to_login">立即登录</a></p>

    </div>
  </div>
</div>
</body>
</html>