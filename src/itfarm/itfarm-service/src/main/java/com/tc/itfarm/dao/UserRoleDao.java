package com.tc.itfarm.dao;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.model.UserRole;
import com.tc.itfarm.model.UserRoleCriteria;

public interface UserRoleDao extends SingleTableDao<UserRole, UserRoleCriteria> {
}