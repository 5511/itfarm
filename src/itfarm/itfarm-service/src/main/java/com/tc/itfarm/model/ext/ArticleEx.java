package com.tc.itfarm.model.ext;
/** 
 * @author  wdd 
 * @date 创建时间：2016年7月16日 下午3:56:58 
 */
public class ArticleEx {
	private Integer recordId;
	
	private String title;

	public Integer getRecordId() {
		return recordId;
	}

	public void setRecordId(Integer recordId) {
		this.recordId = recordId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
