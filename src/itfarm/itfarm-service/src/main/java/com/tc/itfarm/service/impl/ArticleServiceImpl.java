package com.tc.itfarm.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.tc.itfarm.api.model.SingleTableDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.tc.itfarm.api.enums.QueryArticleEnum;
import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.api.util.DateUtils;
import com.tc.itfarm.api.util.PageQueryHelper;
import com.tc.itfarm.dao.ArticleDao;
import com.tc.itfarm.model.Article;
import com.tc.itfarm.model.ArticleCriteria;
import com.tc.itfarm.model.ext.ArticleEx;
import com.tc.itfarm.service.ArticleService;

@Service
public class ArticleServiceImpl extends BaseServiceImpl<Article> implements ArticleService {

	@Resource
	private ArticleDao articleDao;
	
	@Override
	public PageList<Article> selectArticleByPage(Page page, Integer categoryId, String title) {
		ArticleCriteria criteria = new ArticleCriteria();
		ArticleCriteria.Criteria innerCriteria = criteria.createCriteria();
		if (categoryId != null && categoryId != 0) {
			innerCriteria.andTypeIdEqualTo(categoryId);
		}
		if (StringUtils.isNotBlank(title)) {
			innerCriteria.andTitleLike("%"+title+"%");
		}
		return PageQueryHelper.queryPage(page, criteria, articleDao, QueryArticleEnum.TITLE.criteria);
	}

	@Override
	protected SingleTableDao getSingleDao() {
		return articleDao;
	}

	@Override
	public Article select(Integer recordId) {
		return articleDao.selectById(recordId);
	}

	@Override
	public void save(Article article) {
		if (article.getRecordId() == null) {
			article.setCreateTime(DateUtils.now());
			article.setModifyTime(DateUtils.now());
			article.setOrderNo(0);
			articleDao.insert(article);
		} else {
			article.setModifyTime(DateUtils.now());
			articleDao.updateByIdSelective(article);
		}
	}

	@Override
	public List<Article> selectArticles(QueryArticleEnum e, int count) {
		ArticleCriteria criteria = new ArticleCriteria();
		Page page = new Page(0, count);
		criteria.setPage(page);
		criteria.setOrderByClause(e.getCriteria());
		return articleDao.selectByCriteria(criteria);
	}

	@Override
	public ArticleEx selectLast(String title) {
		return articleDao.selectLast(title);
	}

	@Override
	public ArticleEx selectNext(String title) {
		return articleDao.selectNext(title);
	}

}
