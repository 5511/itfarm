package com.tc.itfarm.service.impl;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.dao.LogDao;
import com.tc.itfarm.model.Log;
import com.tc.itfarm.model.LogCriteria;
import com.tc.itfarm.service.LogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wangdongdong on 2016/8/17.
 */
@Service
public class LogServiceImpl extends BaseServiceImpl<Log> implements LogService {

    @Resource
    private LogDao logDao;

    @Override
    protected SingleTableDao getSingleDao() {
        return logDao;
    }

    @Override
    public List<Log> selectByType(String type) {
        LogCriteria criteria = new LogCriteria();
        criteria.setOrderByClause(" create_time desc");
        criteria.or().andTypeLike("%" + type + "%");
        return logDao.selectByCriteria(criteria);
    }
}
