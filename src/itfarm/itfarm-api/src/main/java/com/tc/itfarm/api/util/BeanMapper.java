package com.tc.itfarm.api.util;

import org.springframework.beans.BeanUtils;

/**
 * bean工具类 复制
 * Created by wangdongdong on 2016/8/31.
 */
public class BeanMapper {
    private BeanMapper(){}

    public static void copy(Object des, Object source) {
        BeanUtils.copyProperties(source, des);
    }
}
