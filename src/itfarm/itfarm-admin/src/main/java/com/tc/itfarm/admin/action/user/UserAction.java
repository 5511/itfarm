package com.tc.itfarm.admin.action.user;

import com.google.common.collect.Lists;
import com.tc.itfarm.admin.biz.UserBiz;
import com.tc.itfarm.api.common.Codes;
import com.tc.itfarm.api.common.JsonMessage;
import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageInfo;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.api.util.DateUtils;
import com.tc.itfarm.api.util.StringUtil;
import com.tc.itfarm.model.User;
import com.tc.itfarm.service.UserRoleService;
import com.tc.itfarm.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/8/14.
 */
@Controller
@RequestMapping("/user")
public class UserAction {

    @Resource
    private UserService userService;
    @Resource
    private UserRoleService userRoleService;
    @Resource
    private UserBiz userBiz;

    @RequestMapping("list")
    public String list() {
        return "admin/user/user";
    }

    /**
     * 用户管理列表
     *
     * @param page
     * @param rows
     * @param sort
     * @param order
     * @return
     */
    @RequestMapping(value = "/dataGrid", method = RequestMethod.POST)
    @ResponseBody
    public Object dataGrid(String username, String nickname, String startDate, String endDate, Integer page, Integer rows, String sort, String order) {
        PageInfo pageInfo = new PageInfo(page, rows);
        Map<String, Object> condition = new HashMap<String, Object>();

        if (StringUtils.isNotBlank(username)) {
            condition.put("username", username);
        }
        pageInfo.setCondition(condition);
        PageList<User> userPageList = userService.selectUserByPage(username, nickname, DateUtils.strToDate(startDate), DateUtils.strToDate(endDate), new Page(page, rows));
        pageInfo.setRows(userBiz.getUserVOList(userPageList.getData()));
        pageInfo.setTotal(userPageList.getPage().getTotalRecords());
        return pageInfo;
    }

    @RequestMapping("addUI")
    public String addUI() {
        return "admin/user/userAdd";
    }

    @RequestMapping("editUI")
    public String editUI(ModelMap model, @RequestParam(value = "id", required = true) Integer id) {
        User user = userService.select(id);
        model.addAttribute(user);
        model.addAttribute("roleIds", Lists.newArrayList(userRoleService.selectIdsByUser(id)));
        return "admin/user/userEdit";
    }

    @RequestMapping("save")
    @ResponseBody
    public JsonMessage save(User user, Integer [] roleIds) {
        Integer result = userService.save(user, roleIds);
        return JsonMessage.toResult(result, JsonMessage.STATUS_SUCCESS_MSG, "保存失败");
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public JsonMessage delete(@RequestParam(value = "id", required = true) Integer id) {
        Integer result = userService.delete(id);
        return JsonMessage.toResult(result, JsonMessage.STATUS_SUCCESS_MSG, "删除失败");
    }

}
