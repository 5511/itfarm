<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ITFARM后台管理中心</title>
<meta name="Copyright" content="Douco Design." />
<link href="${ctx }/css/public.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="${ctx }/js/jquery.min.js"></script>
<script type="text/javascript" src="${ctx }/js/global.js"></script>
</head>
<body>
<div id="dcWrap"> 
<jsp:include page="header.jsp"></jsp:include>
 <div id="dcMain"> <!-- 当前位置 -->
<div id="urHere">ITFARM 管理中心</div>  <div id="index" class="mainBox" style="padding-top:18px;height:auto!important;height:550px;min-height:550px;">
    
   <div id="douApi"></div>
      <div class="indexBox">
    <div class="boxTitle">单页面快速管理</div>
    <ul class="ipage">
     <c:forEach items="${menus}" var="item">
     <a href="${ctx}${item.url}" class="child1">${item.name}</a>
     </c:forEach>
    </ul>
   </div>
   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="indexBoxTwo">
    <tr>
     <td width="65%" valign="top" class="pr">
      <div class="indexBox">
       <div class="boxTitle">网站基本信息</div>
       <ul>
        <table width="100%" border="0" cellspacing="0" cellpadding="7" class="tableBasic">
         <tr>
          <td width="120">单页面数：</td>
          <td><strong>${systemConfig.webMenuCount}</strong></td>
          <td width="100">文章总数：</td>
          <td><strong>${systemConfig.articleCount}</strong></td>
         </tr>
         <tr>
          <td>web项目名：</td>
          <td><strong>${systemConfig.webName}</strong></td>
          <td>系统语言：</td>
          <td><strong>${systemConfig.language}</strong></td>
         </tr>
         <tr>
          <td>admin项目名：</td>
          <td><strong>${systemConfig.adminName}</strong></td>
          <td>编码：</td>
          <td><strong>${systemConfig.encoding}</strong></td>
         </tr>
         <tr>
          <td>前台标题：</td>
          <td><strong>${systemConfig.webTitle}</strong></td>
          <td>后台标题：</td>
          <td><strong>${systemConfig.adminTitle}</strong></td>
         </tr>
         <tr>
          <td>上一次修改人：</td>
          <td><strong>${systemConfig.lastModifyUser}</strong></td>
          <td>修改日期：</td>
          <td><strong>${systemConfig.modifyTime}</strong></td>
         </tr>
        </table>
       </ul>
      </div>
     </td>
     <td valign="top" class="pl">
      <div class="indexBox">
       <div class="boxTitle" >管理员  登录记录</div>
       <ul style="height: 200px; overflow: scroll" >
        <table width="100%" border="0" cellspacing="0" cellpadding="7" class="tableBasic">
         <tr>
          <th width="30%">IP地址</th>
          <th width="30%">登录人</th>
          <th width="40%">操作时间</th>
         </tr>
         <c:forEach items="${logs}" var="item">
                  <tr>
          <td align="center">${item.newContent}</td>
          <td align="center">${item.username}</td>
          <td align="center">${item.createTime}</td>
         </tr>
         </c:forEach>
                 </table>
       </ul>
      </div>
     </td>
    </tr>
   </table>
   <div class="indexBox">
    <div class="boxTitle">服务器信息</div>
    <ul>
     <table width="100%" border="0" cellspacing="0" cellpadding="7" class="tableBasic">
      <tr>
       <td width="120" valign="top">PHP 版本：</td>
       <td valign="top">5.3.29 </td>
       <td width="100" valign="top">MySQL 版本：</td>
       <td valign="top">5.5.40</td>
       <td width="100" valign="top">服务器操作系统：</td>
       <td valign="top">WINNT(127.0.0.1)</td>
      </tr>
      <tr>
       <td valign="top">文件上传限制：</td>
       <td valign="top">2M</td>
       <td valign="top">GD 库支持：</td>
       <td valign="top">是</td>
       <td valign="top">Web 服务器：</td>
       <td valign="top">Apache/2.4.10 (Win32) OpenSSL/0.9.8zb mod_fcgid/2.3.9</td>
      </tr>
     </table>
    </ul>
   </div>
  </div>
 </div>
 <div class="clear"></div>
 <jsp:include page="footer.jsp"></jsp:include>
</body>
</html>