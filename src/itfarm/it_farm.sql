/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50528
Source Host           : localhost:3306
Source Database       : it_farm

Target Server Type    : MYSQL
Target Server Version : 50528
File Encoding         : 65001

Date: 2016-08-31 13:45:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_article
-- ----------------------------
DROP TABLE IF EXISTS `t_article`;
CREATE TABLE `t_article` (
  `record_id` int(32) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `title_img` varchar(200) NOT NULL,
  `author_id` int(32) NOT NULL,
  `type_id` int(32) NOT NULL,
  `content` varchar(21299) NOT NULL,
  `keyword` varchar(100) DEFAULT NULL,
  `page_view` int(32) NOT NULL,
  `order_no` int(32) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modify_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_category
-- ----------------------------
DROP TABLE IF EXISTS `t_category`;
CREATE TABLE `t_category` (
  `record_id` int(32) NOT NULL AUTO_INCREMENT,
  `parent_id` int(32) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(32) NOT NULL,
  `modify_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_category
-- ----------------------------
INSERT INTO `t_category` VALUES ('1', null, '程序人生', '2016-08-15 15:32:54', '1', '2016-08-15 15:32:53');
INSERT INTO `t_category` VALUES ('2', null, '技术', '2016-08-15 15:32:57', '6', '2016-08-15 15:32:56');
INSERT INTO `t_category` VALUES ('3', null, '扯淡', '2016-08-15 15:32:59', '1', '2016-08-15 15:32:58');

-- ----------------------------
-- Table structure for t_log
-- ----------------------------
DROP TABLE IF EXISTS `t_log`;
CREATE TABLE `t_log` (
  `record_id` int(32) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL COMMENT '日志类型',
  `old_content` varchar(10000) DEFAULT NULL COMMENT '修改前内容',
  `new_content` varchar(10000) DEFAULT NULL COMMENT '修改后内容',
  `user_id` int(32) NOT NULL COMMENT '操作人员id',
  `username` varchar(100) NOT NULL COMMENT '操作人员用户名',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_log
-- ----------------------------

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu` (
  `record_id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `url` varchar(50) NOT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `modify_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES ('1', '首页', '/article/list.do', 'HOME', '2016-08-29 18:58:59', '2016-08-29 18:58:59');
INSERT INTO `t_menu` VALUES ('2', '程序人生', '/', null, '2016-08-30 08:57:35', '2016-08-30 08:57:38');
INSERT INTO `t_menu` VALUES ('3', '知识问答', '/', null, '2016-08-30 08:58:33', '2016-08-30 08:58:35');
INSERT INTO `t_menu` VALUES ('4', '轻松一下', '/', null, '2016-08-30 08:59:23', '2016-08-30 08:59:22');
INSERT INTO `t_menu` VALUES ('5', '职场励志', '/', null, '2016-08-30 09:00:24', '2016-08-30 09:00:17');

-- ----------------------------
-- Table structure for t_privilege
-- ----------------------------
DROP TABLE IF EXISTS `t_privilege`;
CREATE TABLE `t_privilege` (
  `record_id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) DEFAULT NULL,
  `privilege_code` varchar(20) NOT NULL,
  `privilege_name` varchar(200) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `icon_cls` varchar(20) DEFAULT NULL,
  `resourcetype` int(1) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modify_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `record_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modify_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '用户', '普通用户', '2016-08-15 15:24:33', '2016-08-15 15:24:32');
INSERT INTO `t_role` VALUES ('2', '管理员', '管理员', '2016-08-15 15:24:36', '2016-08-15 15:24:34');
INSERT INTO `t_role` VALUES ('3', '超级管理员', '拥有一切权限', '2016-08-16 10:31:39', '2016-08-16 10:31:39');

-- ----------------------------
-- Table structure for t_role_privilege
-- ----------------------------
DROP TABLE IF EXISTS `t_role_privilege`;
CREATE TABLE `t_role_privilege` (
  `record_id` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NOT NULL,
  `privilege_id` int(10) NOT NULL,
  `privilege_code` varchar(20) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_system_config
-- ----------------------------
DROP TABLE IF EXISTS `t_system_config`;
CREATE TABLE `t_system_config` (
  `record_id` int(32) NOT NULL AUTO_INCREMENT,
  `web_url` varchar(200) NOT NULL DEFAULT '' COMMENT '访问地址',
  `admin_url` varchar(200) NOT NULL DEFAULT '',
  `web_name` varchar(20) NOT NULL COMMENT 'web项目名',
  `admin_name` varchar(20) NOT NULL COMMENT 'admin项目名',
  `web_title` varchar(100) NOT NULL COMMENT 'web标题',
  `admin_title` varchar(100) NOT NULL COMMENT '后台标题',
  `web_menu_count` int(32) NOT NULL COMMENT 'web菜单数',
  `admin_menu_count` int(32) NOT NULL COMMENT '后台菜单数',
  `article_count` int(32) NOT NULL COMMENT '文章数量',
  `language` varchar(100) NOT NULL COMMENT '系统语言',
  `encoding` varchar(20) NOT NULL COMMENT '编码',
  `modify_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '安装时间',
  `last_modify_user` varchar(100) DEFAULT NULL COMMENT '最后修改人',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_system_config
-- ----------------------------
INSERT INTO `t_system_config` VALUES ('1', 'http://localhost:8888/itfarm-web/', 'http://localhost:8080/itfarm-admin/', 'itfarm-web', 'itfarm-admin', '程序猿的世界，你不懂', 'ITFARM后台管理中心', '0', '0', '0', 'zh_cn', 'UTF-8', '2016-08-31 11:31:09', '2016-08-29 10:37:43', '爱吃猫的鱼');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `record_id` int(32) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL COMMENT '用户名',
  `telephone` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `sex` int(1) NOT NULL COMMENT '性别',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  `status` int(1) NOT NULL COMMENT '状态（1、正常 2、停用）',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `photo` varchar(200) DEFAULT NULL COMMENT '头像',
  `email` varchar(200) DEFAULT NULL COMMENT '邮箱',
  `qq` varchar(200) DEFAULT NULL COMMENT 'qq',
  `nickname` varchar(200) DEFAULT NULL COMMENT '昵称',
  `register_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modify_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'wdd', '18888888888', '2', '202CB962AC59075B964B07152D234B70', '22', '1', '安徽灵璧1', null, 'asdds', '812908087', '爱吃猫的鱼1', '2016-08-31 10:57:35', '2016-08-31 10:57:35');

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
  `record_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES ('4', '1', '1', '2016-08-31 10:57:35');
INSERT INTO `t_user_role` VALUES ('5', '1', '2', '2016-08-31 10:57:35');
INSERT INTO `t_user_role` VALUES ('6', '1', '3', '2016-08-31 10:57:35');
